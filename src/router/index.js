import Vue from 'vue'
import VueRouter from 'vue-router'
import Tabla from '../views/Tabla.vue'
import Baraja from '../views/Baraja.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Tabla',
    component: Tabla,
    props: true
  },  
  {
    path: '/baraja',
    name: 'Baraja',
    component: Baraja,
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
